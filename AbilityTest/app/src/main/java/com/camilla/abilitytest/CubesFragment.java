package com.camilla.abilitytest;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

public class CubesFragment extends Fragment implements View.OnClickListener {

    TextView info;
    ImageView patterns;
    RadioButton ansOne;
    RadioButton ansTwo;
    RadioButton ansThree;
    RadioButton ansFour;
    Button next;
    int points;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cubes, container, false);

        info = (TextView) view.findViewById(R.id.cubes_information);
        patterns = (ImageView) view.findViewById(R.id.cubes_image);
        ansOne = (RadioButton) view.findViewById(R.id.cubes_answer_one);
        ansTwo = (RadioButton) view.findViewById(R.id.cubes_answer_two);
        ansThree = (RadioButton) view.findViewById(R.id.cubes_answer_three);
        ansFour = (RadioButton) view.findViewById(R.id.cubes_answer_four);
        next = (Button) view.findViewById(R.id.cubes_next_button);

        next.setOnClickListener(this);

        taskOne();

        return view;
    }

    @Override
    public void onClick(View view) {
        if(info.getText().toString().equals(getString(R.string.cubes_info_one))) {
            points += checkCorrect(ansFour);
            setFalse();
            taskTwo();
        }
        else if(info.getText().toString().equals(getString(R.string.cubes_info_three))) {
            points += checkCorrect(ansTwo);
            setFalse();
            taskThree();
        }
        else if(info.getText().toString().equals(getString(R.string.cubes_info_two))) {
            points += checkCorrect(ansThree);
            ((FragmentContainer)getActivity()).setPerceptionScore(points);
            ((FragmentContainer)getActivity()).changeFragment(new DifferenceFragment());
        }

    }

    void taskOne() {
        info.setText(R.string.cubes_info_one);
        patterns.setImageResource(R.drawable.all_patterns);
        ansOne.setBackgroundResource(R.drawable.pattern_c);
        ansTwo.setBackgroundResource(R.drawable.pattern_b);
        ansThree.setBackgroundResource(R.drawable.pattern_d);
        ansFour.setBackgroundResource(R.drawable.pattern_a);
    }

    void taskTwo() {
        info.setText(R.string.cubes_info_three);
        patterns.setImageResource(R.drawable.connection);
        ansOne.setBackgroundResource(R.drawable.connection_d);
        ansTwo.setBackgroundResource(R.drawable.connection_a);
        ansThree.setBackgroundResource(R.drawable.connection_c);
        ansFour.setBackgroundResource(R.drawable.connection_b);
    }

    void taskThree() {
        info.setText(R.string.cubes_info_two);
        patterns.setImageResource(R.drawable.cubes);

        ansOne.setBackgroundResource(0);
        ansTwo.setBackgroundResource(0);
        ansThree.setBackgroundResource(0);
        ansFour.setBackgroundResource(0);

        ansOne.setText(R.string.cubes_two_one);
        ansTwo.setText(R.string.cubes_two_two);
        ansThree.setText(R.string.cubes_two_three);
        ansFour.setText(R.string.cubes_two_four);
    }

    void setFalse() {
        ansOne.setChecked(false);
        ansTwo.setChecked(false);
        ansThree.setChecked(false);
        ansFour.setChecked(false);
    }

    int checkCorrect(RadioButton rad) {
        if(rad.isChecked()) {
            return 10;
        }
        return 0;
    }
}
