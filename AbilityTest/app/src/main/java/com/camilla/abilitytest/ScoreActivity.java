package com.camilla.abilitytest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class ScoreActivity extends Activity {

    TextView verbal;
    TextView perception;
    int maxVerbalPoints;
    int maxPerceptionPoints;
    int verbalPoints;
    int perceptionPoints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        verbal = (TextView) findViewById(R.id.verbal_score);
        perception = (TextView) findViewById(R.id.perception_score);

        maxVerbalPoints = 40;
        maxPerceptionPoints = 40;

        Intent intent = getIntent();
        verbalPoints = intent.getIntExtra("Verbal", 20);
        perceptionPoints = intent.getIntExtra("Perception", 20);

        verbal.setText(verbalPoints + "/" + maxVerbalPoints);
        perception.setText(perceptionPoints + "/" + maxPerceptionPoints);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_score, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
