package com.camilla.abilitytest;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class FragmentContainer extends Activity {

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    int verbalScore;
    int perceptionScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_container);

        VerbalUnderstanding verbFrag = new VerbalUnderstanding();

        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.add(R.id.container, verbFrag);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fragment_container, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void changeFragment(Fragment frag) {
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, frag);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void setVerbalScore(int score) {
        verbalScore += score;
    }

    public void setPerceptionScore(int score) {
        perceptionScore += score;
    }

    public void showResult() {
        Intent intent = new Intent(this, ScoreActivity.class);
        intent.putExtra("Verbal", verbalScore);
        intent.putExtra("Perception", perceptionScore);
        startActivity(intent);
    }
}
