package com.camilla.abilitytest;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;


public class VerbalUnderstanding extends Fragment implements View.OnClickListener {

    TextView info;
    TextView quest;
    RadioButton ansOne;
    RadioButton ansTwo;
    RadioButton ansThree;
    RadioButton ansFour;
    Button next;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_verbal_understanding, container, false);

        info = (TextView) view.findViewById(R.id.information);
        quest = (TextView) view.findViewById(R.id.question);

        ansOne = (RadioButton) view.findViewById(R.id.answer_one);
        ansTwo = (RadioButton) view.findViewById(R.id.answer_two);
        ansThree = (RadioButton) view.findViewById(R.id.answer_three);
        ansFour = (RadioButton) view.findViewById(R.id.answer_four);

        next = (Button) view.findViewById(R.id.next_button);
        next.setOnClickListener(this);

        taskOne();

        return view;
    }

    @Override
    public void onClick(View view) {
        int points;

        if(info.getText().toString().equals(getString(R.string.info_one))) {

            points = checkCorrect(ansTwo);
            setFalse();
            taskTwo();
        }
        else if(info.getText().toString().equals(getString(R.string.info_two))) {
            points = checkCorrect(ansFour);
            setFalse();
            taskThree();
        }
        else if(info.getText().toString().equals(getString(R.string.info_three))) {
            points = checkCorrect(ansOne);
            setFalse();
            taskFour();
        }
        else if(info.getText().toString().equals(getString(R.string.info_four))) {
            points = checkCorrect(ansTwo);
            ((FragmentContainer)getActivity()).setVerbalScore(points);
            ((FragmentContainer)getActivity()).changeFragment(new CubesFragment());
        }

    }

    void taskOne(){
        info.setText(R.string.info_one);
        quest.setText(R.string.question_one);

        ansOne.setText(R.string.answer_one_one);
        ansTwo.setText(R.string.answer_one_two);
        ansThree.setText(R.string.answer_one_three);
        ansFour.setText(R.string.answer_one_four);
    }

    void taskTwo() {
        info.setText(R.string.info_two);
        quest.setText(R.string.question_two);

        ansOne.setText(R.string.answer_two_one);
        ansTwo.setText(R.string.answer_two_two);
        ansThree.setText(R.string.answer_two_three);
        ansFour.setText(R.string.answer_two_four);
    }

    void taskThree() {
        info.setText(R.string.info_three);
        quest.setText(R.string.question_three);

        ansOne.setText(R.string.answer_three_one);
        ansTwo.setText(R.string.answer_three_two);
        ansThree.setText(R.string.answer_three_three);
        ansFour.setText(R.string.answer_three_four);
    }

    void taskFour() {
        info.setText(R.string.info_four);
        quest.setText(R.string.question_four);

        ansOne.setText(R.string.answer_four_one);
        ansTwo.setText(R.string.answer_four_two);
        ansThree.setText(R.string.answer_four_three);
        ansFour.setText(R.string.answer_four_four);
    }

    void setFalse() {
        ansOne.setChecked(false);
        ansTwo.setChecked(false);
        ansThree.setChecked(false);
        ansFour.setChecked(false);
    }

    int checkCorrect(RadioButton rad) {
        if(rad.isChecked()) {
            return 10;
        }
        return 0;
    }
}