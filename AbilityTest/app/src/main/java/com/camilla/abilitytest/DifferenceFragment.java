package com.camilla.abilitytest;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

public class DifferenceFragment extends Fragment implements View.OnClickListener {

    ImageView teddy;
    ImageView eye;
    TextView info;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_difference, container, false);

        teddy = (ImageView) view.findViewById(R.id.remember);
        eye = (ImageView) view.findViewById(R.id.left_eye);
        info = (TextView) view.findViewById(R.id.difference);

        eye.setOnClickListener(this);

        return view;
    }

    public void onClick(View view) {
        ((FragmentContainer)getActivity()).setPerceptionScore(10);
        ((FragmentContainer)getActivity()).showResult();
    }
}
